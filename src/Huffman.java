
import java.util.*;
import java.util.PriorityQueue;
import java.util.Comparator;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {
   // TODO!!! Your instance variables here!
   HashMap<Byte,String> table = new HashMap<Byte,String>();
   byte[] original;

   /** Constructor to build the Huffman code for a given bytearray.
    * @param origData source data
    */
   Huffman (byte[] origData) {
      if(origData == null)throw new RuntimeException("NULL DETECTED");
      // TODO!!! Your constructor here!
      this.original = origData.clone();
   }

   class HuffmanNode {
      int sum;
      byte code;
      HuffmanNode left;
      HuffmanNode right;
   }

   // For comparing the nodes
   class ImplementComparator implements Comparator<HuffmanNode> {
      public int compare(HuffmanNode x, HuffmanNode y) {
         return x.sum - y.sum;
      }
   }

   //Reference: https://www.geeksforgeeks.org/counting-frequencies-of-array-elements/
   public static int[] countFreq(byte[] arr) {
      boolean[] visited = new boolean[arr.length];
      List<Integer> freq = new ArrayList<>();
      Arrays.fill(visited, false);
      // Traverse through array elements and
      // count frequencies
      for (int i = 0; i < arr.length; i++) {
         // Skip this element if already processed
         if (visited[i])
            continue;
         // Count frequency
         int count = 1;
         for (int j = i + 1; j < arr.length; j++) {
            if (arr[i] == arr[j]) {
               visited[j] = true;
               count++;
            }
         }
         freq.add(count);
      }
      int[] result = new int[freq.size()];
      for(int i=0; i< freq.size(); i++){
         result[i] = freq.get(i);
      }
      return result;
   }

   public static byte[] usedAlphabet(byte[] nums) {
      List<Byte> alphabet = new ArrayList<>();
      for(int c = 48; c <= 90; ++c){
         for (byte num : nums) {
            if (num == c) {
               alphabet.add(num);
               break;
            }
         }
      }
      byte[] result = new byte[alphabet.size()];
      int i=0;
      for (Byte aByte : alphabet) {
         result[i] = aByte;
         i++;
      }
      return result;
   }

   public static void sortByFreq(int[] freq, byte[] alphabet){
      int[] index = new int[freq.length];
      for(int i=0; i< freq.length; i++){
         index[i] = i;
      }

      int tmp1; byte tmp2;
      for (int i = 0; i < freq.length; i++) {
         for (int j = i+1; j < freq.length; j++) {
            if(freq[i] < freq[j]) {
               tmp1 = freq[i];
               freq[i] = freq[j];
               freq[j] = tmp1;

               tmp1 = index[i];
               index[i] = index[j];
               index[j] = tmp1;
            }
         }
      }
      for(int i=0; i< index.length; i++){
         tmp2 = alphabet[i];
         alphabet[i] = alphabet[index[i]];
         alphabet[index[i]] = tmp2;
      }

   }

   public void printCode(HuffmanNode parent, String s) {
      if(parent == null)return;
      if (parent.left == null && parent.right == null)  {
         if(!this.table.containsKey(parent.code)) {
            System.out.println(parent.code + ":" + s);
            this.table.put(parent.code,s);
         }
         return;
      }
      printCode(parent.right, s + "1");
      printCode(parent.left, s + "0");
   }

   /** Length of encoded data in bits. 
    * @return number of bits
    */
   public int bitLength() {
      int result=0;
      for (byte b : original) {
         for (Byte key : table.keySet()) {
            if (b == key) {
               result += table.get(key).length();
               break;
            }
         }
      }
      return result; // TODO!!!
   }

   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
      byte[] alphabet = usedAlphabet(origData);
      int[] freq = countFreq(origData);
      sortByFreq(freq,alphabet);
      System.out.println("ALPH:"+ Arrays.toString(alphabet));
      System.out.println("FREQ:"+ Arrays.toString(freq));
      PriorityQueue<HuffmanNode> queue = new PriorityQueue<HuffmanNode>(alphabet.length, new ImplementComparator());

      //Create nodes
      for (int i = 0; i < alphabet.length; i++) {
         HuffmanNode hn = new HuffmanNode();

         hn.code = alphabet[i];
         hn.sum = freq[i];

         hn.left = null;
         hn.right = null;

         queue.add(hn);
      }

      HuffmanNode root = null;

      //Pull out nodes from queue
      while (queue.size() > 1) {
         HuffmanNode x = queue.peek();
         queue.poll();

         HuffmanNode y = queue.peek();
         queue.poll();

         HuffmanNode f = new HuffmanNode();

         f.sum= x.sum + y.sum;
         f.code = '-';
         f.left = x;
         f.right = y;
         root = f;

         queue.add(f);
      }

      if(alphabet.length == 1){
         this.table.put(alphabet[0],"0");
      }

      printCode(root, "");
      System.out.println(this.table);

      for (int i=0; i<origData.length; i++) {
         if(this.table.containsKey(origData[i])){
            origData[i] = (byte)Short.parseShort(this.table.get(origData[i]),2);
         }
      }

      System.out.println("ENCODED:"+ Arrays.toString(origData));
      return origData; // TODO!!!
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {
      byte[] result = new byte[encodedData.length];
      for(int i=0; i<encodedData.length; i++){
         for(Byte key : table.keySet()){
            if((byte)Short.parseShort(table.get(key),2)==encodedData[i]){
               encodedData[i] = key;
               break;
            }
         }
      }

      System.out.println("DECODED"+Arrays.toString(encodedData));
      return encodedData;
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "1234567890ABCDEF";
      byte[] orig = tekst.getBytes();
      System.out.println("ORIGINAL:"+Arrays.toString(orig));
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
//       must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int length = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + length);
      // TODO!!! Your tests here!
   }
}

//Reference
// Huffman code
//https://www.programiz.com/dsa/huffman-coding
// How huffman code works
//https://www.youtube.com/watch?v=dM6us854Jk0&list=WL&index=2&t=418s